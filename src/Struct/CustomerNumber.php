<?php

declare(strict_types=1);

namespace TeuLoginViaNumber\Struct;

use Shopware\Core\Framework\Struct\Struct;

class CustomerNumber extends Struct
{
    public mixed $customerNumber;

    public function getCustomerNumber(): mixed
    {
        return $this->customerNumber;
    }

    public function setCustomerNumber(mixed $customerNumber): void
    {
        $this->customerNumber = $customerNumber;
    }
}
