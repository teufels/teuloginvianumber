<?php

declare(strict_types=1);

namespace TeuLoginViaNumber\Core\Checkout\Customer;

use Shopware\Core\Framework\Context;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Write\CloneBehavior;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenContainerEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\AggregationResult\AggregationResultCollection;

class CustomerRepository extends EntityRepository
{
    private EntityRepository $inner;
    private SystemConfigService $systemConfigService;

    public function __construct(
        EntityRepository $inner,
        SystemConfigService $systemConfigService
    ) {
        $this->inner = $inner;
        $this->systemConfigService = $systemConfigService;
    }

    public function getDefinition(): EntityDefinition
    {
        return $this->inner->getDefinition();
    }

    public function search(
        Criteria $criteria,
        Context $context,
        bool $searchForCustomerNumber = true
    ): EntitySearchResult {
        $result = $this->inner->search($criteria, $context);

        if ($result->getTotal() > 0 || !$searchForCustomerNumber) {
            return $result;
        }

        $customerNumber = null;

        foreach ($criteria->getFilters() as $filter) {
            if (
                $filter instanceof EqualsFilter &&
                ($filter->getField() === 'email' || $filter->getField() === 'customer.email')
            ) {
                $customerNumber = $filter->getValue();
                break;
            }
        }

        if (is_null($customerNumber)) {
            return $result;
        }

        $customField = $this->systemConfigService->get('TeuLoginViaNumber.config.customField');
        if (!empty($customField)) {
            $criteria = new Criteria();
            $criteria->addFilter(new EqualsFilter('customFields.' . $customField, $customerNumber));
            $criteria->addFilter(new EqualsFilter('guest', 0));
            $criteria->setLimit(1);

            $result = $this->search($criteria, $context, false);
        }

        if ($result->getTotal() == 0) {
            $criteria = new Criteria();
            $criteria->addFilter(new EqualsFilter('customerNumber', $customerNumber));
            $criteria->addFilter(new EqualsFilter('guest', 0));
            $criteria->setLimit(1);

            $result = $this->search($criteria, $context, false);
        }

        return $result;
    }

    public function aggregate(Criteria $criteria, Context $context): AggregationResultCollection
    {
        return $this->inner->aggregate($criteria, $context);
    }

    public function searchIds(Criteria $criteria, Context $context): IdSearchResult
    {
        return $this->inner->searchIds($criteria, $context);
    }

    public function update(array $data, Context $context): EntityWrittenContainerEvent
    {
        return $this->inner->update($data, $context);
    }

    public function upsert(array $data, Context $context): EntityWrittenContainerEvent
    {
        return $this->inner->upsert($data, $context);
    }

    public function create(array $data, Context $context): EntityWrittenContainerEvent
    {
        return $this->inner->create($data, $context);
    }

    public function delete(array $ids, Context $context): EntityWrittenContainerEvent
    {
        return $this->inner->delete($ids, $context);
    }

    public function createVersion(string $id, Context $context, ?string $name = null, ?string $versionId = null): string
    {
        return $this->inner->createVersion($id, $context, $name, $versionId);
    }

    public function merge(string $versionId, Context $context): void
    {
        $this->inner->merge($versionId, $context);
    }

    public function clone(
        string $id,
        Context $context,
        ?string $newId = null,
        ?CloneBehavior $behavior = null
    ): EntityWrittenContainerEvent {
        return $this->inner->clone($id, $context, $newId, $behavior);
    }
}
