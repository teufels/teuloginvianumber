<?php

declare(strict_types=1);

namespace TeuLoginViaNumber\Core\Checkout\Customer\SalesChannel;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Util\Random;
use Symfony\Component\HttpFoundation\RequestStack;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\RateLimiter\RateLimiter;
use Shopware\Core\System\SalesChannel\SuccessResponse;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Checkout\Customer\Event\PasswordRecoveryUrlEvent;
use Shopware\Core\Checkout\Customer\Exception\CustomerNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Checkout\Customer\Event\CustomerAccountRecoverRequestEvent;
use Shopware\Core\Checkout\Customer\Aggregate\CustomerRecovery\CustomerRecoveryEntity;
use Shopware\Core\Checkout\Customer\SalesChannel\AbstractSendPasswordRecoveryMailRoute;

class SendPasswordRecoveryMailRoute extends AbstractSendPasswordRecoveryMailRoute
{
    private AbstractSendPasswordRecoveryMailRoute $inner;
    private EntityRepository $customerRepository;
    private SystemConfigService $systemConfigService;
    private EntityRepository $customerRecoveryRepository;
    private EventDispatcherInterface $eventDispatcher;
    private RequestStack $requestStack;
    private RateLimiter $rateLimiter;

    public function __construct(
        AbstractSendPasswordRecoveryMailRoute $inner,
        EntityRepository $customerRepository,
        SystemConfigService $systemConfigService,
        EntityRepository $customerRecoveryRepository,
        EventDispatcherInterface $eventDispatcher,
        RequestStack $requestStack,
        RateLimiter $rateLimiter
    ) {
        $this->inner = $inner;
        $this->customerRepository = $customerRepository;
        $this->systemConfigService = $systemConfigService;
        $this->customerRecoveryRepository = $customerRecoveryRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->requestStack = $requestStack;
        $this->rateLimiter = $rateLimiter;
    }

    public function getDecorated(): AbstractSendPasswordRecoveryMailRoute
    {
        return $this->inner->getDecorated();
    }

    public function sendRecoveryMail(
        RequestDataBag $data,
        SalesChannelContext $context,
        bool $validateStorefrontUrl = true
    ): SuccessResponse {
        if (filter_var($data->get('email'), FILTER_VALIDATE_EMAIL)) {
            return $this->inner->sendRecoveryMail($data, $context, $validateStorefrontUrl);
        }

        if (($request = $this->requestStack->getMainRequest()) !== null) {
            $this->rateLimiter->ensureAccepted(
                RateLimiter::RESET_PASSWORD,
                strtolower($data->get('email') . '-' . $request->getClientIp())
            );
        }

        $customer = $this->getCustomerByNumber($data->get('email'), $context);
        $customerId = $customer->getId();

        $customerIdCriteria = new Criteria();
        $customerIdCriteria->addFilter(new EqualsFilter('customerId', $customerId));
        $customerIdCriteria->addAssociation('customer.salutation');

        $repoContext = $context->getContext();

        if ($existingRecovery = $this->customerRecoveryRepository->search($customerIdCriteria, $repoContext)->first()) {
            $this->deleteRecoveryForCustomer($existingRecovery, $repoContext);
        }

        $recoveryData = [
            'customerId' => $customerId,
            'hash' => Random::getAlphanumericString(32),
        ];

        $this->customerRecoveryRepository->create([$recoveryData], $repoContext);

        $customerRecovery = $this->customerRecoveryRepository->search($customerIdCriteria, $repoContext)->first();

        $hash = $customerRecovery->getHash();

        $recoverUrl = $this->getRecoverUrl($context, $hash, $data->get('storefrontUrl'), $customerRecovery);

        $event = new CustomerAccountRecoverRequestEvent($context, $customerRecovery, $recoverUrl);
        $this->eventDispatcher->dispatch($event, CustomerAccountRecoverRequestEvent::EVENT_NAME);

        return new SuccessResponse();
    }

    private function getCustomerByNumber(string $customerNumber, SalesChannelContext $context): CustomerEntity
    {
        $customField = $this->systemConfigService->get('TeuLoginViaNumber.config.customField');

        $baseCriteria = new Criteria();
        $baseCriteria->addFilter(new EqualsFilter('customer.active', 1));
        $baseCriteria->addFilter(new EqualsFilter('customer.guest', 0));

        $baseCriteria->addFilter(new MultiFilter(MultiFilter::CONNECTION_OR, [
            new EqualsFilter('customer.boundSalesChannelId', null),
            new EqualsFilter('customer.boundSalesChannelId', $context->getSalesChannel()->getId()),
        ]));

        $result = null;
        if (!empty($customField)) {
            $criteria = clone $baseCriteria;
            $criteria->addFilter(new EqualsFilter('customer.customFields.' . $customField, $customerNumber));

            $result = $this->customerRepository->search($criteria, $context->getContext());
        }

        if (is_null($result) || $result->count() !== 1) {
            $criteria = clone $baseCriteria;
            $criteria->addFilter(new EqualsFilter('customer.customerNumber', $customerNumber));

            $result = $this->customerRepository->search($criteria, $context->getContext());
        }

        if ($result->count() !== 1) {
            throw new CustomerNotFoundException($customerNumber);
        }

        return $result->first();
    }

    private function deleteRecoveryForCustomer(CustomerRecoveryEntity $existingRecovery, Context $context): void
    {
        $recoveryData = [
            'id' => $existingRecovery->getId(),
        ];

        $this->customerRecoveryRepository->delete([$recoveryData], $context);
    }

    private function getRecoverUrl(
        SalesChannelContext $context,
        string $hash,
        string $storefrontUrl,
        CustomerRecoveryEntity $customerRecovery
    ): string {
        $urlTemplate = $this->systemConfigService->get(
            'core.loginRegistration.pwdRecoverUrl',
            $context->getSalesChannelId()
        );
        if (!\is_string($urlTemplate)) {
            $urlTemplate = '/account/recover/password?hash=%%RECOVERHASH%%';
        }

        $urlEvent = new PasswordRecoveryUrlEvent($context, $urlTemplate, $hash, $storefrontUrl, $customerRecovery);
        $this->eventDispatcher->dispatch($urlEvent);

        return rtrim($storefrontUrl, '/') . str_replace(
            '%%RECOVERHASH%%',
            $hash,
            $urlEvent->getRecoveryUrl()
        );
    }
}
