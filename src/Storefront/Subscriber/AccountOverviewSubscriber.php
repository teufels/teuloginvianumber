<?php

declare(strict_types=1);

namespace TeuLoginViaNumber\Storefront\Subscriber;

use TeuLoginViaNumber\Struct\CustomerNumber;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Storefront\Page\Account\Overview\AccountOverviewPageLoadedEvent;

class AccountOverviewSubscriber implements EventSubscriberInterface
{
    private SystemConfigService $systemConfigService;

    public function __construct(SystemConfigService $systemConfigService)
    {
        $this->systemConfigService = $systemConfigService;
    }

    public static function getSubscribedEvents()
    {
        return [
            AccountOverviewPageLoadedEvent::class => 'onAccountOverviewPageLoaded'
        ];
    }

    public function onAccountOverviewPageLoaded(AccountOverviewPageLoadedEvent $event)
    {
        $customField = $this->systemConfigService->get('TeuLoginViaNumber.config.customField');

        if (!empty($customField)) {
            $customer = $event->getSalesChannelContext()->getCustomer();
            $customFields = $customer->getCustomFields() ?: [];

            if (array_key_exists($customField, $customFields)) {
                $customerNumber = $customFields[$customField];

                if (!empty($customerNumber)) {
                    $data = new CustomerNumber();
                    $data->setCustomerNumber($customerNumber);

                    $page = $event->getPage();
                    $page->addExtension('teu_customer_number_login', $data);
                }
            }
        }
    }
}
